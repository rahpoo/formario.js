$(document).ready(function () {
    $(document)
        .on('mouseover', '.form-row, .form-row *', function () {
            var activeFormRow = $(this).closest('.form-row');

            if (!activeFormRow.hasClass('active-row')) {
                activeFormRow
                    .addClass('active-row')
                    .find('.row-actions')
                    .animate({
                        left: '-23px'
                    }, 100, function () {
                        $(this).css('zIndex', 1);
                    });
            }
        })
        .on('mouseleave', '.form-row', function () {
            var activeFormRow = $(this).closest('.form-row');

            activeFormRow
                .removeClass('active-row')
                .find('.row-actions')
                .css('zIndex', '-1')
                .stop()
                .animate({
                    left: 0
                }, 50);
        });


    $(document)
        .on('mouseover', '.row-actions, .row-actions *', function () {
            var activeRowActions = $(this).closest('.row-actions');

            if (!activeRowActions.hasClass('active-row-actions')) {
                activeRowActions.addClass('active-row-actions');

                if (!activeRowActions.hasClass('open')) {
                    var a_items_h = activeRowActions.find('.action-items > li').length * 24;

                    activeRowActions
                        .find('.action-items')
                        .css('display', 'block')
                        .animate({
                            height: a_items_h + "px",
                        }, 100)
                        .addClass('open');

                    activeRowActions
                        .find('span')
                        .animate({
                            width: 'auto',
                            left: '23px',
                        }, 100);
                }
            }
        })
        .on('mouseleave', '.row-actions', function () {
            var activeRowActions = $(this).closest('.row-actions');

            activeRowActions
                .removeClass('active-row-actions')
                .find('.action-items')
                .removeClass('open')
                .animate({
                    height: '24px',
                }, 100, function () {
                    $(this).hide();
                });

            activeRowActions
                .find('span')
                .animate({
                    width: 0,
                    left: 0,
                }, 100, function () {
                    $(this).hide();
                });

        });



    $(document)
        .on('mouseover', '.col-actions, .col-actions *', function () {
            var activeColActions = $(this).closest('.col-actions');

            if (!activeColActions.hasClass('active-col-actions')) {
                activeColActions.addClass('col-actions');

                if (!activeColActions.hasClass('open')) {
                    activeColActions.find('.action-items').show();
                    activeColActions
                        .addClass('open')
                        .find('span')
                        .animate({
                            top: "-25px",
                        }, 100, function () {
                            $(this).show();
                        });
                }
            }
        })
        .on('mouseleave', '.col-actions', function () {
            $(this).resetColumn();
        });


    $(document)
        .on('mouseover', '.form-group, .form-group *', function () {
            var activeFieldGroup = $(this).closest('.form-group');

            if (!activeFieldGroup.hasClass('active-form-group')) {
                activeFieldGroup
                    .addClass('active-form-group')
                    .find('.action-items').show();
            }
        })
        .on('mouseleave', '.form-group', function () {
            var activeFieldGroup = $(this).closest('.form-group');

            activeFieldGroup
                .removeClass('active-form-group')
                .find('.action-items').hide();
        })
        .on('mouseover', '.field-actions, .field-actions *', function () {
            var activeFieldGroup = $(this).closest('.field-actions');

            if (!activeFieldGroup.hasClass('active-field-actions')) {
                activeFieldGroup
                    .addClass('active-field-actions')
                    .find('.action-items').animate({
                    width: '98px',
                    left: '-74px'
                }, 100);
            }
        })
        .on('mouseleave', '.field-actions', function () {
            $(this).resetFieldGroup();
        });
});
"use strict";

(function($) {
    $.fn.formArio = function(options) {
        var $this = this;
        var defaults = {
            wizard: true,
            maxSteps: 7,
            stepTitle: 'Form Step {number}',
            stepNewTitle: 'Untitled Step'
        };
        var config = $.extend({}, defaults, options);

        if (this.length > 1) {
            this.each(function() { $(this).formArio(options) });
            return this;
        }

        // private variables
        var obj = {
            builder: false,
            activeTab: false,
            pagination: false,
            lastPlace: false,
            draggedItem: false,
            page: $('.form-page'),
            row: $('.form-row'),
            col: $('.form-col'),
            field: $('.form-group'),

            newRow: function () {
                var new_row = $('<div>').addClass('form-row');
                var row_actions = $('<div>').addClass('row-actions');
                var action_items = '<li><a href="#" class="move-action move-row"><i class="ifa i-move"></i></a></li>' +
                    '<li><a href="#" class="edit-action"><i class="ifa i-edit"></i></a></li>' +
                    '<li><a href="#" class="copy-action"><i class="ifa i-copy"></i></a></li>' +
                    '<li><a href="#" class="remove-action"><i class="ifa i-remove"></i></a></li>';

                new_row.append('<div class="place-holder-caption">Row</div>');

                $(row_actions)
                    .append(
                        '<i class="ifa i-handle-v"></i>' +
                        '<span>Row</span>' +
                        '<ul class="action-items"></ul>'
                    )
                    .find('.action-items')
                    .append(action_items);

                new_row.append(row_actions);

                return new_row;
            },
            newCol: function () {
                var new_col = $('<div>').addClass('form-col col-sm-12');
                var col_actions = $('<div>').addClass('col-actions');
                var action_items = '<li><a href="#" class="copy-action"><i class="ifa i-copy"></i></a></li>' +
                    '<li><a href="#" class="move-action move-col"><i class="ifa i-move"></i></a></li>' +
                    '<li><a href="#" class="edit-action"><i class="ifa i-edit"></i></a></li>' +
                    '<li><a href="#" class="remove-action"><i class="ifa i-remove"></i></a></li>';

                new_col.append('<div class="place-holder-caption">Column</div>');
                col_actions
                    .append(
                        '<i class="ifa i-handle-h"></i>' +
                        '<span>Column</span>' +
                        '<ul class="action-items"></ul>'
                    )
                    .find('.action-items')
                    .append(action_items);

                new_col.append(col_actions);

                return $(new_col);
            },
            newField: function () {
                var new_field = $('<div>').addClass('form-field');
                var field_actions = $('<div>').addClass('field-actions');
                var action_items = '<li><a href="#" class="copy-action"><i class="ifa i-copy"></i></a></li>' +
                    '<li><a href="#" class="move-action move-field"><i class="ifa i-move"></i></a></li>' +
                    '<li><a href="#" class="edit-action"><i class="ifa i-edit"></i></a></li>' +
                    '<li><a href="#" class="remove-action"><i class="ifa i-remove"></i></a></li>';

                $(field_actions)
                    .append(
                        '<i class="ifa i-handle-h"></i>' +
                        '<ul class="action-items"></ul>'
                    )
                    .find('.action-items')
                    .append(action_items);

                return $(new_field).append(field_actions);
            }
        };




        $.fn.sortablePage = function () {
            $(this).sortable({
                group: 'form-rows',
                handle: '.move-row',
                swapThreshold: 1,
                animation: 100,
                forceFallback: true,

                onStart: function (e) {
                    obj.draggedItem = $(e.item);
                },

                onEnd: function (e) {
                    func.refreshItems();
                },

                onMove: function (e) {
                    if (!$(e.to).hasClass('form-page') && $(e.dragged).hasClass('form-row')) {
                        return false;
                    }
                },

                onChange: function (e) {
                    if ($(e.item).hasClass('form-col')) {
                        $(e.item).css({
                            float: 'none',
                            width: '100%'
                        });
                    }
                },

                onAdd: function (e) {
                    var item = $(e.item);

                    if (!item.hasClass('form-row')) {
                        var newItem = item.clone();

                        if (item.hasClass('form-group')) {
                            newItem.sortableCol();
                            newItem = obj.newCol().append(newItem);
                        }

                        newItem = obj.newRow().append(newItem);
                        $(e.item).replaceWith(newItem);

                        newItem.sortableRow();
                        newItem.find('.form-col').sortableCol();

                        $(newItem).find('.form-col').css({
                            float: 'none',
                            width: '100%'
                        });

                        func.prepare.rows($(e.from));
                    }
                }
            });
        };
        $.fn.sortableRow = function () {
            $(this).sortable({
                group: 'form-rows',
                handle: '.move-col',
                draggable: ".form-col",
                ignore: '.form-row',
                swapThreshold: 0.85,
                animation: 150,
                forceFallback: true,

                onStart: function (e) {
                    obj.draggedItem = $(e.item);
                },

                onEnd: function (e) {
                    func.refreshItems();
                },

                onMove: function (e) {
                    if ($(e.to).hasClass('form-col') && $(e.dragged).hasClass('form-col')) {
                        return false;
                    }

                    if ($(e.to).hasClass('form-row') && $(e.to).find('.form-col').length == 12) {
                        return false;
                    }
                },

                onChange: function (e) {
                    var newItem = $(e.item).clone();
                    obj.lastPlace || $(e.from);

                    if (obj.lastPlace) {
                        func.prepare.rows(obj.lastPlace);
                    }

                    if (e.from == e.to) {
                        var cols = $(e.to).find('.form-col');
                        var total_cols = cols.length - 1;

                        if (total_cols <= 0 && obj.lastPlace) {
                            total_cols = 1;
                        }

                        func.prepare.rows($(e.to), total_cols);
                    } else {
                        func.prepare.rows($(e.to));
                    }

                    obj.lastPlace = $(e.to);
                    $(e.item).sortableCol();
                    $(newItem).sortableRow();
                    func.refreshItems($(e.item));
                },

                onAdd: function (e) {
                    var item = $(e.item);

                    if ($(e.to).hasClass('form-row')) {
                        if (item.hasClass('form-col')) {
                            func.prepare.rows($(e.to));
                        }
                    }

                    func.prepare.rows($(e.from));
                }
            });
        };
        $.fn.sortableCol = function () {
            $(this).sortable({
                group: 'form-rows',
                handle: '.move-field',
                draggable: '.form-group',
                swapThreshold: 1,
                animation: 150,
                forceFallback: true,

                onStart: function (e) {
                    obj.draggedItem = $(e.item);
                },

                onEnd: function (e) {
                    func.refreshItems();
                },

                onMove: function (e) {
                    if ($(e.to).hasClass('form-group')) {
                        return false;
                    }

                    if ($(e.to).hasClass('form-row') && $(e.dragged).hasClass('form-group')) {
                        return false;
                    }
                },

                onChange: function (e) {
                    func.refreshItems($(e.item));
                }
            });
        };



        $.fn.sortablePagination = function () {
            $(this).sortable({
                group: {
                    name: 'form-page',
                    pull: false
                },
                draggable: 'li',
                swapThreshold: 1,
                animation: 50
            });
        };

        var func = {
            refreshItems: function () {
                $(document).find('.form-page').each(function () {
                    if ($(this).find('.form-row').length > 0) {
                        $(this).removeClass('empty');
                    } else {
                        $(this).addClass('empty');
                    }
                });
                $(document).find('.form-row').each(function () {
                    var cols = $(this).find('.form-col');

                    if (cols.length == 0) {
                        $(this).detach();
                    }
                    /*else {
                        var _height = 0;

                        $(this).find('.form-col').each(function () {
                            var col_height = 0;
                            $(this).find('.form-group').each(function () {
                                col_height += $(this).outerHeight();
                            });
                            $(this).css({height: col_height + 'px'});

                            if (col_height > _height) {
                                _height = col_height + 6;
                            }
                        });

                        $(this).css({height: _height + 'px'});
                    }*/
                });

                obj.draggedItem = false;
            },
            editableItem: function (item, config) {
                var defaults = {
                    event : 'dblclick',
                    touch : true,
                    lineBreaks : false,
                    toggleFontSize : true,
                    closeOnEnter : true,
                    emptyMessage : false,
                    editorStyle : {}
                };
                config = $.extend({}, defaults, config);
                item.editable(config);
            },

            prepare: {
                pages: function () {
                    var pages = obj.builder.find('.form-page');
                    var firstPage = pages.first();

                    if (pages.length > 0) {
                        $(firstPage).addClass('tab-pane active');
                    } else {
                        firstPage = $('<div>').addClass('form-page active empty');
                        obj.builder.append(firstPage);
                        pages = obj.builder.find('.form-page');
                    }

                    if (config.wizard === true) {
                        var tab_content = obj.builder.find('.tab_content');

                        if (tab_content.length == 0) {
                            var tab_content = $('<div>').addClass('tab-content clearfix');
                            obj.builder.append(tab_content);
                        }

                        $(tab_content).append(pages);
                        func.register.pagination(pages);
                        obj.pagination.sortablePagination();
                    }

                    $(pages).each(function () {
                        var rows = obj.builder.find('.form-row');

                        if (rows.length > 0) {
                            $(firstPage).removeClass('empty').append(rows);
                        }

                        $(this).sortablePage();
                    });

                    obj.page = $(firstPage);

                    $(document).ready(function () {
                        $(document).on('shown.bs.tab', '[data-toggle="tab"]', function (e) {
                            obj.activeTab = $(document).find($(e.target).attr('href'));
                        });

                        $(this).on('mouseover', '.form-pager', function () {
                            if (!$(this).closest('li').hasClass('active') && obj.draggedItem) {
                                var item = obj.draggedItem;
                                $(this).tab('show');

                                if (item.hasClass('form-col')) {
                                    item.resetColumn().toggleClass('[className^="col-"]', 'col-sm-12');
                                    item = obj.newRow().append(item);
                                    item.sortableRow();
                                } else if (item.hasClass('form-group')) {
                                    item.resetFieldGroup();
                                    item = obj.newCol().append(item);
                                    item.sortableCol();
                                    item = obj.newRow().append(item);
                                    item.sortableRow();
                                }

                                $(obj.activeTab).append(item).sortablePage();
                            }
                        });
                    });
                },

                rows: function (rows, total_cols) {
                    rows = rows || $('.form-page.active').find('.form-row');

                    rows.each(function () {
                        var cols = $(this).find('.form-col');

                        if (!total_cols) {
                            total_cols = cols.length;
                        }

                        if (total_cols == 0 && $(this).hasClass('form-row')) {
                            $(this).detach();
                            return;
                        }

                        var col_size = 100 / total_cols;
                        cols.css({
                            float: 'left',
                            width: col_size + '%'
                        });
                    });
                }
            },
            register: {
                pagination: function (pages) {
                    var total_s = 0;
                    var pagination = $('<div>').attr('id', 'form-pagination');
                    var formTabs = $('<ul>').addClass('nav nav-tabs');
                    var adder = $('<a>')
                        .attr('href', '#')
                        .attr('rel', 'add-new-form-page')
                        .append('+');

                    pagination.append(adder);
                    pages.each(function (num) {
                        var li = $('<li>');
                        num += 1;
                        var page_id = 'form-step-' + num;

                        if (num == 1) {
                            li.addClass('active');
                        }

                        li.append('<a href="#' + page_id + '" class="form-pager" data-toggle="tab"><span>' + config.stepTitle.replace('{number}', num) + '</span></a>');
                        formTabs.append(li);

                        $(this).attr('id', page_id).addClass('tab-pane');
                    });

                    pagination.append(formTabs);
                    obj.builder.prepend(pagination);
                    obj.pagination = $('#form-pagination .nav-tabs');

                    $('body').on('click', '[rel="add-new-form-page"]', function () {
                        var pages = obj.builder.find('.form-page');
                        var page_num = pages.length + 1;
                        var newPage = $('<div>').addClass('tab-pane form-page empty').attr('id', 'form-step-' + page_num);
                        var newTab = $('<li>').addClass('page-handle');

                        $('#form-builder .tab-content').append(newPage);
                        newTab.append('<a href="#form-step-' + page_num + '" class="form-pager" data-toggle="tab"></a>');
                        newTab.find('a').append('<span> ' + config.stepTitle.replace('{number}', page_num) + '</span>');
                        obj.pagination.append(newTab);

                        func.editableItem(newTab.find('span'),{
                            callback: function(data) {
                                if(data.content == '') {
                                    var content = config.stepNewTitle + (total_s > 0 ? ' ' + total_s : '');
                                    data.$el.html(content);
                                    total_s++;
                                }
                            }
                        });
                    });

                    var pager = obj.pagination.find('li a span');

                    if (pager.length > 0) {
                        func.editableItem(pager,{
                            callback: function(data) {
                                if(data.content == '') {
                                    var content = config.stepNewTitle + (total_s > 0 ? ' ' + total_s : '');
                                    data.$el.html(content);
                                    total_s++;
                                }
                            }
                        });
                    }
                },

                formActions: function () {
                    $('body').on('click', '.action-items a', function () {
                        if ($(this).hasClass('move-action')) {
                            return;
                        }

                        var action = $(this).attr('class').split('-')[0];
                        var item = $(this).parents().get(3);

                        switch (action) {
                            case 'remove':
                                $(item).detach();
                                break;

                            case 'copy':
                                var newItem = $(item).clone();

                                if ($(item).hasClass('form-col')) {
                                    var row = $(item).parents('.form-row');

                                    if (row.find('.form-col').length == 12) {
                                        return false;
                                    }

                                    newItem.resetColumn().sortableCol();
                                    row.append(newItem);
                                    func.prepare.rows(row);
                                } else if ($(item).hasClass('form-row')) {
                                    $(item).parents('.form-page').append(newItem);
                                    newItem.resetRow().sortableRow();
                                    newItem.find('.form-col').sortableCol();
                                }
                                break;
                        }

                        func.refreshItems();
                    });
                }
            }
        };


        this.initialize = function() {
            obj.builder = $(this);

            func.prepare.pages();

            obj.page.sortablePage();
            obj.row.sortableRow();
            obj.col.sortableCol();

            func.register.formActions();

            return this;
        };

        return this.initialize();
    }
})(jQuery);

$.fn.resetRow = function () {
    var actions = $(this).find('.row-actions');

    if ($(this).hasClass('form-row')) {
        $(this).removeClass('active-row');
        actions
            .css('zIndex', '-1')
            .stop()
            .animate({
                height: '24px',
                left: 0
            }, 0);
    }

    actions
        .removeClass('active-row-actions')
        .find('.action-items')
        .removeClass('open')
        .animate({
            height: '24px',
        }, 0, function () {
            $(this).hide();
        });

    actions
        .find('.action-items span')
        .animate({
            width: 0,
            left: 0
        }, 0, function () {
            $(this).hide();
        });


    return this;
};
$.fn.resetColumn = function () {
    var cla = $(this);

    if ($(this).hasClass('form-col')) {
        cla = cla.find('.col-actions');
    }

    cla
        .removeClass('open')
        .removeClass('active-col-actions')
        .find('span')
        .animate({
            top: 0,
        }, 100, function () {
            $(this).hide();
        });

    cla.find('.action-items').fadeOut();

    return this;
};
$.fn.resetFieldGroup = function () {
    var newfg = false;
    var fga = $(this);

    if ($(this).hasClass('form-group')) {
        newfg = true;
        fga = fga.removeClass('active-form-group').find('.field-actions');
    }

    fga
        .removeClass('active-field-actions')
        .find('.action-items')
        .animate({
            width: '24px',
            left: 0
        }, 100, function () {
            if (newfg) {
                $(this).hide();
            }
        });

    return this;
};;